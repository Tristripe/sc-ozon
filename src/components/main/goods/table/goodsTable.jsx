import React, { useMemo, useState, useEffect, useCallback } from "react";
import GlobalSearch from "../../control/globalSearch";
import ContextMenu from "./cells/contextMenu";
import { useGlobalFilter, useSortBy, useTable, useFilters, usePagination } from "react-table";
import Filters from "../../control/filters/filters";
import useGoods from "../../../../hooks/useGoods";
import BottomPagination from "../../pagination/bottomPagination";
import COLUMNS from "./columns";
import { moveToDeleted, updateTab } from "../../../../api/api";
import cn from "classnames";
import { addNotification } from "../../../../utils/Notification";
import FileUpload from "../../fileUpload/fileUpload";

const activeTabs = [
  {
    title: "Новинка",
    tabId: "new",
    tabName: "Новинки",
  },
  {
    title: "Актуальный",
    tabId: "current",
    tabName: "Действующая матрица",
  },
  {
    title: "Архив",
    tabId: "archive",
    tabName: "Архив",
  },
];

const GoodsTable = () => {
  const [goods, setGoods] = useGoods([]);
  const [activeTabsState, setActiveTab] = useState(activeTabs[0]);
  const [filteredData, setFilteredData] = useState([]);

  const columns = useMemo(() => COLUMNS, []);
  const data = useMemo(() => filteredData, [filteredData]);

  const tableInstance = useTable(
    {
      columns,
      data,
      initialState: {
        hiddenColumns: ["tab"],
      },
    },
    useFilters,
    useGlobalFilter,
    useSortBy,
    usePagination
  );

  useEffect(() => {
    const { filters } = tableInstance.state;
    const filteredGoods = goods.filter((row) => row.isDeleted === false && row.tab === activeTabsState.title);
    setFilteredData(filteredGoods);
  }, [activeTabsState.title, goods, tableInstance]);

  /**
   * Переключаемся между новинками, действующей матрицей и архивом
   */
  const changeActiveTab = (activeTab) => {
    setActiveTab(activeTab);
  };

  /**
   * Изменяем характеристику товара: Актуальный, Новинка, Архив
   */
  const changeProductTab = async (tabValue, rowId) => {
    try {
      await updateTab(tabValue, rowId);
      const newGoodsList = goods.map((item) => {
        if (item.id === rowId) {
          item.tab = tabValue;
        }
        return item;
      });

      setGoods(newGoodsList);
      addNotification("success", "Успех!", "Товар был перенесен");
    } catch (e) {
      addNotification("danger", "Ошибка!", "Перенести не удалось");
    }
  };

  /**
   * Пометить товар в базе на удаление
   */
  const markAsDeleted = async (rowId) => {
    try {
      await moveToDeleted(true, rowId);

      const newGoodsList = goods.map((item) => {
        if (item.id === rowId) {
          item.isDeleted = true;
        }
        return item;
      });

      setGoods(newGoodsList);
      addNotification("success", "Успех!", "Товар был удален");
    } catch (e) {
      addNotification("danger", "Ошибка!", "Удалить не удалось");
    }
  };

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    page,
    nextPage,
    previousPage,
    canNextPage,
    canPreviousPage,
    setPageSize,
    prepareRow,
    state,
    setGlobalFilter,
    pageOptions,
    gotoPage,
    setFilter,
  } = tableInstance;

  const { globalFilter } = state;

  return (
    <>
      <div className="container" id="goods">
        <div className="block block__title">
          <h1 className="title title__main">Товары</h1>
          <FileUpload goods={goods} />
        </div>
      </div>
      <div className="container container--fullscreen">
        <div className="block block__content container">
          <div className="block__wrapper">
            {activeTabs.map(({ tabId, tabName, title }) => (
              <button
                key={tabId}
                type="button"
                onClick={() => changeActiveTab({ title, tabId, tabName })}
                className={cn("btn btn__main-filter", {
                  "btn--black": activeTabsState.tabId === tabId,
                })}
              >
                {tabName}
              </button>
            ))}
          </div>
        </div>
        <div className="block goods goods--scroll">
          <div className="container">
            <GlobalSearch filter={globalFilter} setFilter={setGlobalFilter} />
            <Filters setFilter={setFilter} />
          </div>
          <div className="goods__table-wrapper">
            <table className="table goods__table" {...getTableProps()}>
              <thead>
                {headerGroups.map((headerGroup) => {
                  return (
                    <tr {...headerGroup.getHeaderGroupProps()}>
                      <td />
                      {headerGroup.headers.map((column) => (
                        <td {...column.getHeaderProps(column.getSortByToggleProps())}>
                          <button
                            className={cn("arrow-btn", {
                              "arrow-btn--up": columns.isSortedDesc,
                              "arrow-btn--down": !columns.isSortedDesc,
                            })}
                            type="button"
                          >
                            {column.render("Header")}
                          </button>
                        </td>
                      ))}
                    </tr>
                  );
                })}
              </thead>
              <tbody {...getTableBodyProps()}>
                {page.map((row) => {
                  prepareRow(row);
                  return (
                    <tr {...row.getRowProps()}>
                      <td className="table__dots-cell dots-btn">
                        <ContextMenu rowId={row.original.id} changeTab={changeProductTab} deleteTab={markAsDeleted} />
                      </td>
                      {row.cells.map((cell, index) => {
                        return (
                          <td {...cell.getCellProps()}>
                            {cell.render("Cell", {
                              rowId: row.original.id,
                              accessor: COLUMNS[index].accessor,
                              ratios: row.original.ratios,
                            })}
                          </td>
                        );
                      })}
                    </tr>
                  );
                })}
              </tbody>
            </table>
          </div>
          <BottomPagination
            setPageSize={setPageSize}
            nextPage={nextPage}
            previousPage={previousPage}
            canNextPage={canNextPage}
            canPreviousPage={canPreviousPage}
            pageOptions={pageOptions}
            gotoPage={gotoPage}
          />
        </div>
      </div>
    </>
  );
};

export default GoodsTable;
