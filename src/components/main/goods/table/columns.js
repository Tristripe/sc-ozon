import EdisCell from "./cells/edisCell";
import RatioCell from "./cells/ratioCell";

const COLUMNS = [
  {
    Header: "ID Товара",
    accessor: "id",
    id: "ID товара",
  },
  {
    Header: "Ozon",
    accessor: "sku",
    id: "Ozon",
  },
  {
    Header: "Название",
    accessor: "name",
    id: "Название товара",
  },
  {
    Header: "Тип кабинета",
    accessor: "account",
    id: "Тип кабинета",
  },
  {
    Header: "Категория EDIS",
    accessor: "isEdis",
    id: "Категория EDIS",
    Cell: EdisCell,
  },
  {
    Header: "Категория товара",
    accessor: "level",
    id: "Категория товара",
  },
  {
    Header: "Товарная категория",
    accessor: "department",
    id: "Товарная категория",
  },
  {
    Header: "Коэфф. промо",
    accessor: "ratios.promo",
    id: "Коэфф. промо",
    Cell: RatioCell,
  },
  {
    Header: "Коэфф. спец.цены",
    accessor: "ratios.special",
    id: "Коэфф. cпец.цены",
    Cell: RatioCell,
  },
  {
    Header: "Коэфф. промокодов",
    accessor: "ratios.code",
    id: "Коэфф. промокодов",
    Cell: RatioCell,
  },
  {
    Header: "Коэфф. оборачиваемости",
    accessor: "ratios.turn",
    id: "Коэфф. оборачиваемости",
    Cell: RatioCell,
  },
  {
    Header: "Categories",
    accessor: "tab",
  },
];

export default COLUMNS;
