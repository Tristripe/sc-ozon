import React, { useEffect, useRef, useState } from "react";
import useOnClickOutside from "use-onclickoutside";
import { updateTab } from "../../../../../api/api";

const ContextMenu = ({ rowId, changeTab, deleteTab }) => {
  const [open, setOpen] = useState(false);
  const ref = useRef(null);

  const handleClickOutside = () => {
    setOpen(false);
  };

  const handleClickInside = () => {
    setOpen(true);
  };

  const onTabClick = (tabValue, rowId) => {
    changeTab(tabValue, rowId);
    setOpen(false);
  };

  const onRowDelete = (rowId) => {
    deleteTab(rowId);
    setOpen(false);
  };

  useOnClickOutside(ref, handleClickOutside);

  return (
    <div ref={ref}>
      <button
        onClick={handleClickInside}
        className="dots-btn__button dots-btn__button--active goods__context"
        type="button"
      >
        •
      </button>
      <div
        className={`dropdown__list dropdown__list--dots ${
          open ? "active" : ""
        }`}
      >
        <ul className="dots-btn__list">
          <li>
            <button
              className="dots-btn__option"
              onClick={() => onTabClick("Новинка", rowId)}
              type="button"
            >
              Перенести в новинки
            </button>
          </li>
          <li>
            <button
              className="dots-btn__option"
              onClick={() => onTabClick("Актуальный", rowId)}
              type="button"
            >
              Перенести в действующую матрицу
            </button>
          </li>
          <li>
            <button
              className="dots-btn__option"
              onClick={() => onTabClick("Архив", rowId)}
              type="button"
            >
              Перенести в архив
            </button>
          </li>
          <li>
            <button
              className="dots-btn__option"
              onClick={() => onRowDelete(rowId)}
              type="button"
            >
              Удалить
            </button>
          </li>
        </ul>
      </div>
    </div>
  );
};

export default ContextMenu;
