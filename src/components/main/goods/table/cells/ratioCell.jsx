import React, { useEffect, useState } from "react";
import { instance } from "../../../../../api/api";
import { validateNumberInput } from "../../../../../helpers/validations";

const RatioCell = ({ value: initialValue, rowId, ratios, accessor }) => {
  const [value, setValue] = useState("");
  const [editMode, setEditMode] = useState(false);

  useEffect(() => {
    setValue(initialValue);
  }, [initialValue]);

  const deactivateEditMode = async () => {
    // await
    try {
      await instance.patch(`/goods/${rowId}`, {
        ratios: {
          ...ratios,
          [accessor.split(".").pop()]: parseInt(value),
        },
      });
    } finally {
      setEditMode(false);
    }
  };

  const activeEditMode = () => setEditMode(true);

  const onValueChange = (e) => {
    let { value } = e.currentTarget;
    if (value === "") {
      value = 0;
    }

    if (!validateNumberInput(value)) {
      return;
    }
    setValue(parseInt(value).toString());
  };

  return editMode ? (
    <input className="input input--params" onChange={onValueChange} value={value} onBlur={deactivateEditMode} />
  ) : (
    <p className="input input--params" onDoubleClick={activeEditMode}>
      {value}
    </p>
  );
};

export default RatioCell;
