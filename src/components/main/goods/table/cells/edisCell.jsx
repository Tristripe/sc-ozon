import React, { useState } from "react";
import { instance } from "../../../../../api/api";

const EdisCell = ({ value: initialValue, rowId }) => {
  const [value, setValue] = useState(initialValue);
  const [disabled, setDisabled] = useState(false);

  const changeEdisStatus = async () => {
    try {
      setDisabled(true);
      setValue(!value);
      await instance.patch(`/goods/${rowId}`, { isEdis: !value });
    } finally {
      setDisabled(false);
    }
  };

  return (
    <button
      type="button"
      onClick={changeEdisStatus}
      disabled={disabled}
      className={`switch goods__switch ${value ? "switch--active" : ""}`}
    />
  );
};

export default EdisCell;
