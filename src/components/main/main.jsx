import React, { useEffect, useState } from "react";
import Header from "../header/header";
import { Router, Switch, Route, useHistory } from "react-router-dom";
import Orders from "./orders/orders";
import Goods from "./goods/goods";
import Analytics from "./analytics/analytics";
import Settings from "./settings/settings";
import OrderItem from "./orderItem/orderItem";
import ItemExpanded from "./itemExpanded/table/itemExpanded";
import ReactNotifications from "react-notifications-component";
import "react-notifications-component/dist/theme.css";
import SettingsContext, { initialContextValue } from "../../SettingsContext";
import { instance } from "../../api/api";

const Main = () => {
  const history = useHistory();

  const [settings, setSettings] = useState(initialContextValue);

  useEffect(() => {
    instance.get("/settings").then(({ data }) => setSettings(data));
  }, []);

  return (
    <SettingsContext.Provider value={settings[0]}>
      <ReactNotifications />
      <Header />
      <main className="content-wrapper">
        <section className="section-main">
          <Router history={history}>
            <Switch>
              <Route path="/goods" component={Goods} />
              <Route path="/orders" component={Orders} />
              <Route path="/analytics" component={Analytics} />
              <Route path="/settings" component={Settings} />
              <Route path="/order-item/:id" component={OrderItem} exact />
              <Route path="/item-expanded/:id" component={ItemExpanded} exact />
            </Switch>
          </Router>
        </section>
      </main>
    </SettingsContext.Provider>
  );
};

export default Main;

export const routes = [
  {
    path: "/goods",
    component: Goods,
    exact: true,
  },
];
