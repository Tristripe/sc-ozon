import React, { useEffect, useMemo } from "react";
import { useTable, useSortBy, usePagination } from "react-table";
import { COLUMNS } from "./columns";
import BottomPagination from "../../pagination/bottomPagination";
import { useHistory } from "react-router-dom";
import useOrders from "../../../../hooks/useOrders";

const OrdersTable = () => {
  const [orders, listOrders] = useOrders();

  useEffect(() => {
    listOrders();
  }, []);

  const columns = useMemo(() => COLUMNS, []);
  const data = orders;

  const tableInstance = useTable(
    {
      columns,
      data,
      initialState: {
        hiddenColumns: ["id", "city", "item.sku", "item.name", "item.quantity"],
      },
    },
    useSortBy,
    usePagination
  );

  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    page,
    nextPage,
    previousPage,
    canNextPage,
    canPreviousPage,
    setPageSize,
    prepareRow,
    pageOptions,
    gotoPage,
  } = tableInstance;

  const history = useHistory();
  const handleRowClick = (row) => {
    history.push(`/order-item/${row.original.id}`);
  };

  return (
    <div className="block">
      <div className="goods__wrapper">
        <table className="table table--order" {...getTableProps()}>
          <thead>
            {headerGroups.map((headerGroup) => {
              return (
                <tr {...headerGroup.getHeaderGroupProps()}>
                  {headerGroup.headers.map((column) => (
                    <td {...column.getHeaderProps(column.getSortByToggleProps())}>
                      <button
                        className={
                          column.isSorted
                            ? column.isSortedDesc
                              ? "arrow-btn arrow-btn--up"
                              : "arrow-btn arrow-btn--down"
                            : "arrow-btn"
                        }
                        type="button"
                      >
                        {column.render("Header")}
                      </button>
                    </td>
                  ))}
                </tr>
              );
            })}
          </thead>
          <tbody {...getTableBodyProps()}>
            {page.map((row) => {
              prepareRow(row);
              return (
                <tr
                  {...row.getRowProps()}
                  className={row.values.status === "Подтверждён" ? "table__cell" : "table__cell table__cell--warning"}
                  onClick={() => handleRowClick(row)}
                >
                  {row.cells.map((cell) => {
                    return (
                      <td {...cell.getCellProps()}>
                        {cell.render("Cell", {
                          orderStats: row.original.items,
                        })}
                      </td>
                    );
                  })}
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
      <BottomPagination
        nextPage={nextPage}
        previousPage={previousPage}
        canNextPage={canNextPage}
        canPreviousPage={canPreviousPage}
        setPageSize={setPageSize}
        pageOptions={pageOptions}
        gotoPage={gotoPage}
      />
    </div>
  );
};

export default OrdersTable;
