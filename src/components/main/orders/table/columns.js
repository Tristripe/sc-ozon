import React from "react";

export const COLUMNS = [
  {
    Header: 'id',
    accessor: 'id'
  },
  {
    Header: '№ Заказа',
    accessor: 'orderNumber'
  },
  {
    Header: 'Дата формирования',
    accessor: 'date'
  },
  {
    Header: 'Сумма',
    accessor: 'summ'
  },
  {
    Header: 'Статус',
    accessor: 'status'
  },
  {
    Header: 'Город',
    accessor: 'city'
  }
]
