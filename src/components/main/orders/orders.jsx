import React from "react";
import OrdersTable from "./table/ordersTable";

const Orders = () => {
    return (
        <div className="container" id="orders">
            <div className="block block__title-order">
                <h1 className="title title__main">Заказы</h1>
            </div>
            <OrdersTable/>
        </div>
    )
}

export default Orders;
