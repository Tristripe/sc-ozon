import React, { useState } from "react";

const OrderProducts = (props) => {
  const renderCell = props.items.map((item) => {
    return (
      <tr key={item.sku}>
        <td className="table__sku-cell">{item.sku}</td>
        <td className="table__order-name-cell">{item.name}</td>
        <td className="table__order-price-cell">{item.itemPrice}</td>
        <td className="table__order-quantity-cell">
          <div className="quantity">
            {props.status === "Подтверждён" ? (
              <label>
                <input className="quantity__input" type="text" value={item.quantity} disabled />
              </label>
            ) : (
              <>
                <button
                  className="quantity__minus"
                  type="button"
                  onClick={(e) => props.onChangeCount(e, "decrement", item.sku, item.quantity)}
                >
                  -
                </button>
                <label>
                  <input
                    className="quantity__input"
                    type="text"
                    value={item.quantity}
                    onChange={(e) => props.onInputChange(e, item.sku)}
                  />
                </label>
                <button
                  className="quantity__plus"
                  type="button"
                  onClick={(e) => props.onChangeCount(e, "increment", item.sku, item.quantity)}
                >
                  +
                </button>
              </>
            )}
          </div>
        </td>
        <td className="table__order-summ-cell">{item.quantity * item.itemPrice}</td>
        <td className="table__order-delete-cell">
          {props.status === "Ожидает" && (
            <button className="btn-delete" type="button" onClick={(e) => props.onDeleteProduct(e, item.sku)}>
              Удалить
            </button>
          )}
        </td>
      </tr>
    );
  });

  return (
    <div className="order-list__table-wrapper">
      <table className="table table--order-list order-list__table">
        <thead>
          <tr>
            <td>SKU</td>
            <td>Наименование товара</td>
            <td>Цена за единицу</td>
            <td>Количество</td>
            <td>Общая сумма</td>
            <td />
          </tr>
        </thead>
        <tbody>{renderCell}</tbody>
      </table>
    </div>
  );
};

export default OrderProducts;
