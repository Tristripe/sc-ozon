import React, { useEffect } from "react";
import OrderProducts from "./table/orderProducts";
import { instance } from "../../../api/api";
import { useParams, NavLink } from "react-router-dom";
import { validateNumberInput } from "../../../helpers/validations";

/**
 * @type {{itemId: number, quantity: number, name: string, itemPrice: number, sku: string}}
 */
const cartItemInitial = {
  sku: "",
  name: "",
  quantity: 0,
  itemPrice: 0,
  itemId: 0,
};

const OrderItem = () => {
  const params = useParams();

  /**
   * @type {{summ: number, items: cartItemInitial[], status: 'Ожидает' | 'Подтверждён', loading: boolean}}
   */
  const initialValue = {
    summ: 0,
    items: [],
    status: "Ожидает",
    city: "",
  };
  const [cartState, setCartState] = React.useState(initialValue);

  useEffect(() => {
    instance.get(`/orders/${params.id}`).then(({ data }) => setCartState(data));
  }, []);

  /**
   * @param {React.MouseEvent<HTMLButtonElement>} e
   * @param {string} sku
   */
  const deleteProduct = (e, sku) => {
    e.preventDefault();

    setCartState((prevState) => {
      const items = prevState.items;
      const newItems = items.filter((item) => item.sku !== sku);
      const summ = newItems.reduce((sum, item) => sum + item.itemPrice * item.quantity, 0);

      return {
        ...prevState,
        items: newItems,
        summ,
      };
    });
  };

  /**
   * Изменяет кол-во товаров
   * @param {number} quantity кол-во товара
   * @param {string} sku SKU товара
   */
  const changeProductQtty = (quantity, sku) => {
    setCartState((prevState) => {
      const { items } = prevState;

      items.map((item) => {
        if (item.sku === sku) {
          item.quantity = quantity;
        }
        return item;
      });

      const summ = items.reduce((totalSumm, item) => totalSumm + item.itemPrice * item.quantity, 0);

      return {
        ...prevState,
        items,
        summ,
      };
    });
  };

  /**
   *
   * @param {React.MouseEvent<HTMLButtonElement>} e
   * @param {'increment' | 'decrement'} type
   * @param {string} sku
   * @param {number} quantity
   */
  const changeCount = (e, type, sku, quantity) => {
    e.preventDefault();

    if (type === "increment") {
      return changeProductQtty(quantity + 1, sku);
    }
    if (type === "decrement") {
      if (quantity < 1) {
        return;
      }
      return changeProductQtty(quantity - 1, sku);
    }
  };

  const clearOrder = (e) => {
    e.preventDefault();
    setCartState(initialValue);
  };

  const confirmOrder = async (e, items, summ) => {
    setCartState((prevState) => ({
      ...prevState,
      loading: true,
    }));
    try {
      // await
      await instance.patch(`/orders/${params.id}`, {
        status: "Подтверждён",
        items: items,
        summ: summ,
      });
      setCartState((prevState) => {
        return {
          ...prevState,
          status: "Подтверждён",
        };
      });
    } catch (e) {
      console.log(e);
    }
  };

  const onInputQuantity = (e, sku) => {
    e.preventDefault();
    let { value } = e.target;

    if (value === "") {
      value = 0;
    }

    if (!validateNumberInput(value)) {
      return;
    }

    changeProductQtty(parseInt(value), sku);
  };

  return (
    <div className="container">
      <div className="block block__title block__order-status">
        <h1 className="title title__main">Заказ № {params.id}</h1>
        <p>{cartState.status}</p>
        <NavLink to={`/item-expanded/${params.id}`} className="payment title__payment">
          Посмотреть расчет заказа
        </NavLink>
      </div>
      <div className="block block__order-info order-info">
        <p className="order-info__city">
          Город:<span>{cartState.city}</span>
        </p>
        <p className="order-info__position">
          Количество позиций:<span> {cartState.items.length}</span>
        </p>
        <p className="order-info__summ">
          Сумма заказа:<span> {cartState.summ}</span>
        </p>
      </div>
      <div className="block block__order-list order-list">
        <h2 className="title title__order-list title--subtitle">Информация о заказе</h2>
        <OrderProducts
          items={cartState.items}
          status={cartState.status}
          onDeleteProduct={deleteProduct}
          onChangeCount={changeCount}
          onInputChange={onInputQuantity}
        />
      </div>
      {cartState.status !== "Подтверждён" && (
        <div className="block block__button-wrapper">
          <button className="btn btn__cancel" type="button" onClick={(e) => clearOrder(e)}>
            Отменить заказ
          </button>
          <button
            className="btn btn--black"
            type="button"
            onClick={(e) => confirmOrder(e, cartState.items, cartState.summ)}
            // disabled={cartState.loading}
          >
            Подтвердить заказ
          </button>
        </div>
      )}
    </div>
  );
};

export default OrderItem;
