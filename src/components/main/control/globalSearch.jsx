import React, { useState } from "react";
import cn from "classnames";

const GlobalSearch = ({ filter, setFilter }) => {
  const [filterValue, setFilterValue] = useState("");

  const resetValue = () => {
    setFilter("");
    setFilterValue("");
  };

  const changeFilterValue = (e) => {
    setFilter(e.target.value);
    setFilterValue(e.target.value);
  };

  return (
    <div className="block block__search">
      <h2 className="title title__subtitle title--subtitle">Поиск</h2>
      <div className="block__wrapper">
        <label>
          <input
            value={filter || ""}
            onChange={(e) => changeFilterValue(e)}
            type="text"
            className="input input__search input--search"
            placeholder="Название"
          />
          <button
            className={cn("input__close", {
              "input__close--active": filterValue !== "",
            })}
            onClick={resetValue}
          >
            ×
          </button>
        </label>
      </div>
    </div>
  );
};

export default GlobalSearch;
