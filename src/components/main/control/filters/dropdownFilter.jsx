import React, { useState, useRef } from "react";
import useOnClickOutside from "use-onclickoutside";
import cn from "classnames";

const DropdownFilter = ({ options, placeholder, setFilter }) => {
  const [open, setOpen] = useState(false);
  const ref = useRef(null);
  const [value, setValue] = useState("");

  const chooseOption = (option) => {
    setFilter(placeholder, option);
    setValue(option);
    setOpen(false);
  };

  const handleClickOutside = () => {
    setOpen(false);
  };

  const resetValue = () => {
    setValue("");
    setFilter(placeholder, "");
  };

  const renderedOptions = options.map((option) => {
    if (option === value) {
      return null;
    }

    return (
      <li key={option} onClick={() => chooseOption(option)}>
        <a className="dropdown__link">{option}</a>
      </li>
    );
  });

  useOnClickOutside(ref, handleClickOutside);

  return (
    <div ref={ref} className="dropdown__item" key={placeholder}>
      <input
        onClick={() => setOpen(!open)}
        value={value}
        type="text"
        className="dropdown__button"
        placeholder={placeholder}
        readOnly
      />
      <div className="dropdown__wrapper">
        <ul className={`dropdown__list ${open ? "active" : ""}`}>
          {renderedOptions}
        </ul>
      </div>
      <button
        className={cn("dropdown__close", {
          "dropdown__close--active": value !== "",
        })}
        onClick={resetValue}
      >
        ×
      </button>
    </div>
  );
};

export default DropdownFilter;
