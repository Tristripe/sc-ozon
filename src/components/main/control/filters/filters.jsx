import React, { useEffect, useMemo } from "react";
import useFilters from "../../../../hooks/useFilters";
import InputFilter from "./inputFilter";
import DropdownFilter from "./dropdownFilter";

const Filters = ({ setFilter }) => {
  const [filters, listFilters] = useFilters([]);

  useEffect(() => {
    listFilters();
  }, []);

  const memoFilter = useMemo(() => filters, [filters]);

  return (
    <div className="block block__filter dropdown">
      <h2 className="title title__subtitle title--subtitle">Фильтр</h2>
      <div className="block__wrapper dropdown__wrapper">
        {memoFilter
          .filter((filter) => filter.type === "input")
          .map((input) => (
            <label key={input.name}>
              <InputFilter key={input.id} placeholder={input.name} setFilter={setFilter} />
            </label>
          ))}
        {memoFilter
          .filter((filter) => filter.type === "dropdown")
          .map((dropdown, index) => (
            <DropdownFilter key={index} options={dropdown.items} placeholder={dropdown.name} setFilter={setFilter} />
          ))}
      </div>
    </div>
  );
};

export default Filters;
