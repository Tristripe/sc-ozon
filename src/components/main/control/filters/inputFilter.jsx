import React from "react";

const InputFilter = ({ key, placeholder, setFilter }) => {
  return (
    <>
      <input
        onChange={(e) => setFilter(placeholder, e.target.value)}
        className="input--filter input--ozon input"
        type="text"
        key={key}
        placeholder={placeholder}
      />
    </>
  );
};

export default InputFilter;
