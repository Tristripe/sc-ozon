import React from "react";
import {CSVLink} from "react-csv";

const FileUpload = ({goods}) => {
  return (
    <div className="upload">
      <button className="btn btn--download" type="button">
        <CSVLink data={goods} filename={"goods.xls"}>Скачать файл</CSVLink>
      </button>
      <button className="btn btn__upload btn--upload" type="button">
        Загрузить файл
      </button>
    </div>
  );
};

export default FileUpload;
