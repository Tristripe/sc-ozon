import React, { useCallback, useEffect, useMemo, useState } from "react";
import { useTable } from "react-table";
import COLUMNS from "./columns";
import { instance } from "../../../../api/api";
import { useParams, NavLink } from "react-router-dom";
import { CSVLink } from "react-csv";

const ItemExpanded = () => {
  const [expandedItems, setExpandedItems] = useState([]);
  const [cartState, setCartState] = React.useState([]);
  const params = useParams();

  useEffect(() => {
    instance.get(`/expandedItems/${params.id}`).then(({ data }) => setExpandedItems(data.rows));
  }, []);

  useEffect(() => {
    instance.get(`/orders/${params.id}`).then(({ data }) => setCartState(data));
  }, []);

  const initialValue = {
    itemsLength: 0,
    summ: 0,
  };

  const columns = useMemo(() => COLUMNS, []);
  const data = expandedItems;

  const tableInstance = useTable({
    columns,
    data,
    getSubRows: useCallback((row, id) => {
      return row.rows || [];
    }, []),
  });

  const { getTableProps, getTableBodyProps, headerGroups, rows, prepareRow } = tableInstance;

  return (
    <section className="section-main">
      <div className="container">
        <div className="block block__title block__order-status">
          <h1 className="title title__order">
            <a>Заказ № {params.id}</a>
          </h1>
          <p className="title__status">Новый</p>
          <div className="upload">
            <button className="btn btn--download" type="button">
              <button className="btn btn--download" type="button">
                <CSVLink data={expandedItems} filename={`file-${params.id}.xls`}>
                  Скачать файл
                </CSVLink>
              </button>
            </button>
          </div>
        </div>
        <div className="block block__order-info order-info">
          <p className="order-info__position">
            Количество позиций: {cartState.itemsLength || initialValue.itemsLength}
          </p>
          <p className="order-info__summ">Сумма заказа: {cartState.summ || initialValue.summ}</p>
          <NavLink to={`/order-item/${params.id}`} className="btn btn--right">
            Вернуться к заказу
          </NavLink>
        </div>
      </div>
      <div className="container container--fullscreen">
        <div className="block block__order-list order-list">
          <h2 className="title title__order-list title--subtitle">Информация о заказе</h2>
          <div className="goods__table-wrapper">
            <table className="table goods__table" {...getTableProps()}>
              <thead>
                {headerGroups.map((headerGroup) => {
                  return (
                    <tr {...headerGroup.getHeaderGroupProps()}>
                      {headerGroup.headers.map((column) => (
                        <td {...column.getHeaderProps()}>{column.render("Header")}</td>
                      ))}
                    </tr>
                  );
                })}
              </thead>
              <tbody {...getTableBodyProps()}>
                {rows.map((row) => {
                  prepareRow(row);
                  return (
                    <tr {...row.getRowProps()} className="table__cell">
                      {row.cells.map((cell) => {
                        return <td {...cell.getCellProps()}>{cell.render("Cell")}</td>;
                      })}
                    </tr>
                  );
                })}
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </section>
  );
};

export default ItemExpanded;
