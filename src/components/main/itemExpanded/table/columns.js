const COLUMNS = [
  {
    Header: "abc",
    accessor: "abc",
  },
  {
    Header: "Норматив оборачиваемости",
    accessor: "standardTurnover",
  },
  {
    Header: "Номенклатура",
    accessor: "nomenclature",
  },
  {
    Header: "Артикул",
    accessor: "vendor",
  },
  {
    Header: "Артикул FBS",
    accessor: "fbs",
  },
  {
    Header: "Артикул FBO",
    accessor: "fbo",
  },
  {
    Header: "Продажи за январь 2021 со своего склада",
    accessor: "ownJanuary2021",
  },
  {
    Header: "Продажи за январь 2021 со склада МП",
    accessor: "mpJanuary2021",
  },
  {
    Header: "Продажи за февраль 2021 со своего склада",
    accessor: "ownFebruary2021",
  },
  {
    Header: "Продажи за февраль 2021 со склада МП",
    accessor: "mpFebruary2021",
  },
  {
    Header: "Продажи за март 2021 со своего склада",
    accessor: "ownMarch2021",
  },
  {
    Header: "Продажи за март 2021 со склада МП",
    accessor: "mpMarch2021",
  },
  {
    Header: "Продажи за апрель 2021 со своего склада",
    accessor: "ownApril2021",
  },
  {
    Header: "Продажи за апрель 2021 со склада МП",
    accessor: "mpApril2021",
  },
  {
    Header: "Продажи за май 2021 со своего склада",
    accessor: "ownMay2021",
  },
  {
    Header: "Продажи за май 2021 со склада МП",
    accessor: "mpMay2021",
  },
  {
    Header: "Продажи за июнь 2021 со своего склада",
    accessor: "ownJune2021",
  },
  {
    Header: "Продажи за июнь 2021 со склада МП",
    accessor: "mpJune2021",
  },
  {
    Header: "Прогноз продаж июнь со своего склада",
    accessor: "predictionOwnJune2021",
  },
  {
    Header: "Прогноз продаж июнь со склада МП",
    accessor: "predictionMpJune2021",
  },
  {
    Header: "Ср-мес продажи факт",
    accessor: "averageSalesFact",
  },
  {
    Header: "Ср-мес продажи МП",
    accessor: "averageSalesMp",
  },
  {
    Header: "РРЦ",
    accessor: "rrc",
  },
  {
    Header: "Промо-цена",
    accessor: "promoPrice",
  },
  {
    Header: "Промокод",
    accessor: "promocode",
  },
  {
    Header: "Коэффициент",
    accessor: "coef",
  },
  {
    Header: "Коэффициент ОБЩ",
    accessor: "coefAll",
  },
  {
    Header: "АПП прогноз мес",
    accessor: "AppPredictionMonth",
  },
  {
    Header: "Остаток общий МП",
    accessor: "remainsAllMp",
  },
  {
    Header: "Остаток на складе Поставщика",
    accessor: "remainsAllShipper",
  },
  {
    Header: "В пути на МП",
    accessor: "inProgressMp",
  },
  {
    Header: "Оборачиваемость факт",
    accessor: "factTurnover",
  },
  {
    Header: "Оборачиваемость прогноз",
    accessor: "predictionTurnover",
  },
  {
    Header: "Нормативная оборачиваемость",
    accessor: "averageTurnover",
  },
  {
    Header: "Расчет заказа",
    accessor: "paymentOrder",
  },
  {
    Header: "Заказ к отгрузке с поправкой на склад Мск",
    accessor: "orderMsc",
  },
  {
    Header: "Стоимость РРЦ",
    accessor: "priceRrc",
  },
  {
    Header: "Пополнение склада Мск",
    accessor: "supplyMsc",
  },
];

export default COLUMNS;
