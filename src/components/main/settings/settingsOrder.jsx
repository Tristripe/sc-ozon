import React from "react";
import cn from "classnames";

const SettingsOrder = (props) => {
  return !!props.settings ? (
    <section className="section-half">
      <h2 className="title title__options title--subtitle">Настройки заказа</h2>
      <div className="options options--big">
        <div className="options__row">
          <div className="options__text">
            <p>Автоматическое размещение заказа:</p>
            <p className="options__text--small">
              Возможность автоматического размещения (вне зависимости от суммы заказа)
            </p>
          </div>
          <button
            className="switch switch__options switch--active"
            type="button"
            className={cn("switch switch__options", {
              "switch--active": props.settings.autoComplete,
            })}
          />
        </div>
        <div className="options__row">
          <span className="options__description">Пороговая сумма заказа без подтверждения:</span>
          <label>
            <input className="options__input" type="text" value={props.settings.threshold} readOnly />
          </label>
        </div>
      </div>
    </section>
  ) : (
    <div />
  );
};

export default SettingsOrder;
