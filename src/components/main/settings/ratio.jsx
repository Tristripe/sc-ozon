import React from "react";

const Ratio = (props) => {
  return !!props.settings ? (
    <section className="section-half">
      <h2 className="title title__options title--subtitle">Коэффициенты</h2>
      <div className="options">
        <div className="options__row">
          <span className="options__description">Коэффициент промо:</span>
          <label className="options__label">
            <input
              className="options__input"
              type="text"
              readOnly
              placeholder="Не указан"
              value={props.settings.code}
            />
          </label>
        </div>
        <div className="options__row">
          <span className="options__description">Коэффициент спец.цены:</span>
          <label className="options__label">
            <input
              className="options__input"
              type="text"
              readOnly
              placeholder="Не указан"
              value={props.settings.special}
            />
          </label>
        </div>
        <div className="options__row">
          <span className="options__description">Коэффициент промокодов:</span>
          <label className="options__label">
            <input
              className="options__input"
              type="text"
              readOnly
              placeholder="Не указан"
              value={props.settings.code}
            />
          </label>
        </div>
        <div className="options__row">
          <span className="options__description">Коэффициент оборачиваемости:</span>
          <label className="options__label">
            <input
              className="options__input"
              type="text"
              readOnly
              placeholder="Не указан"
              value={props.settings.turn}
            />
          </label>
        </div>
      </div>
    </section>
  ) : (
    <div />
  );
};

export default Ratio;
