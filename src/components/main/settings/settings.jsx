import React from "react";
import Ratio from "./ratio";
import SettingsOrder from "./settingsOrder";
import SettingsContext from "../../../SettingsContext";

const Settings = (props) => {
  return (
    <SettingsContext.Consumer>
      {(settings) => (
        <div className="container" id="settings">
          <div className="block block__title">
            <h1 className="title title__main">Главный заголовок</h1>
          </div>
          <div className="block block__wrapper block__options">
            <Ratio settings={settings} />
            <SettingsOrder settings={settings} />
          </div>
        </div>
      )}
    </SettingsContext.Consumer>
  );
};

export default Settings;
