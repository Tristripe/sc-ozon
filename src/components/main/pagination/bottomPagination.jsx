import React, { useState } from "react";
import cn from "classnames";

const BottomPagination = ({
  nextPage,
  previousPage,
  canNextPage,
  canPreviousPage,
  setPageSize,
  pageOptions,
  gotoPage,
}) => {
  const [active, setActive] = useState("");
  const [activePagination, setActivePagination] = useState(10);
  const [pageNumber, setPageNumber] = useState(0);

  const setActiveSize = (number) => {
    setPageSize(number);
    setActive(active);
    setActivePagination(number);
  };

  const setActivePage = (i) => {
    gotoPage(i);
    setPageNumber(i);
  };

  const setPrevPage = () => {
    previousPage();
    setPageNumber(pageNumber - 1);
  }

  const setNextPage = () => {
    nextPage();
    setPageNumber(pageNumber + 1);
  }

  const renderButtons = [10, 25, 50, 1000].map((option) => {
    return (
      <button
        key={option}
        className={cn("pagination__button", {
          "pagination__button--active": activePagination === option,
        })}
        onClick={() => setActiveSize(option)}
      >
        {option}
      </button>
    );
  });

  const renderPageButtons = pageOptions.map((i) => {
    return (
      <li key={i}>
        <button
          key={i}
          type="button"
          className={cn("pagination__button", {
            "pagination__button--active": pageNumber === i,
          })}
          onClick={() => setActivePage(i)}
        >
          {i + 1}
        </button>
      </li>
    );
  });

  return (
    <div className="block pagination">
      <div className="pagination__wrapper">
        <div className="pagination__options">{renderButtons}</div>
        <div className="pagination__list-wrapper">
          <button
            className="pagination__button pagination__button--controls"
            onClick={() => setPrevPage()}
            disabled={!canPreviousPage}
          >
            {"<"}
          </button>
          <ul className="pagination__list">{renderPageButtons} </ul>
          <button
            className="pagination__button pagination__button--controls"
            onClick={() => setNextPage()}
            disabled={!canNextPage}
          >
            {">"}
          </button>
        </div>
      </div>
    </div>
  );
};

export default BottomPagination;
