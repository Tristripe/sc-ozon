import React from 'react';
import Main from "./components/main/main";
import Head from "./components/head/head";

const App = () => {
  return (
      <>
            <Head/>
            <div className="app-wrapper">
                <Main/>
            </div>
      </>
  );
}

export default App;
