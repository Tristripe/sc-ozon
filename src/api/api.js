import axios from "axios";

export const instance = axios.create({
  baseURL: "http://localhost:3001",
});

export const updateTab = (tab, id) => {
  return instance.patch(`/goods/${id}`, { tab: tab });
};

export const moveToDeleted = (isDeleted, id) => {
  return instance.patch(`goods/${id}`, { isDeleted: isDeleted });
};

export const deleteItem = (id) => {
  return instance.delete(`orders/${id}`);
};
