export const validateNumberInput = (value) => {
  return /^[0-9]+$/gm.test(value);
};
