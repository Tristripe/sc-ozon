import { useState, useEffect } from "react";
import { instance } from "../api/api";

const useGoods = (initState) => {
  const [goods, setGoods] = useState(initState);

  useEffect(() => {
    instance
      .get("/goods")
      .then((response) => {
        setGoods(response.data);
      })
      .catch((e) => console.log(e));
  }, []);

  return [goods, setGoods];
};

export default useGoods;
