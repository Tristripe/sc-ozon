import { useState } from "react";
import { instance } from "../api/api";

const useExpandedItems = () => {
  const [expandedItems, setExpandedItems] = useState([]);

  const listExpandedItems = async () => {
    const response = await instance.get("/expandedItems/1");
    setExpandedItems(response.data);
  };

  return [expandedItems, listExpandedItems];
};

export default useExpandedItems;
