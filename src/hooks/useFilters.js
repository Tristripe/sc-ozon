import { useState } from "react";
import { instance } from "../api/api";

const useFilters = () => {
  const [filters, setFilters] = useState([]);

  const listFilters = async () => {
    const response = await instance.get("/filters");
    setFilters(response.data);
  };

  return [filters, listFilters];
};

export default useFilters;

/**
[
  {
    "name": "ID товара",
    "type": "input"
  },
  {
    "name": "ID товара ozon",
    "type": "input"
  },
  {
    "name": "Название товара",
    "type": "input"
  },
  {
    "name": "Тип кабинета",
    "type": "dropdown",
    "items": [
      "Мелкогабаритный",
      "Крупногабаритный"
    ]
  },
  {
    "name": "Категория EDIS",
    "type": "dropdown",
    "items": [
      true,
      false
    ]
  },
  {
    "name": "Категория товара",
    "type": "dropdown",
    "items": [
      "A",
      "B",
      "C"
    ]
  },
  {
    "name": "Товарная категория",
    "type": "dropdown",
    "items": [
      1,
      2,
      3,
      4,
      5,
      6,
      7,
      8,
      9,
      10,
      11,
      12,
      13
    ]
  },
  {
    "name": "Коэфф. промо",
    "type": "dropdown",
    "items": [
      0.15,
      0.16,
      0.17
    ]
  },
  {
    "name": "Коэфф. cпец.цены",
    "type": "dropdown",
    "items": [
      0.15,
      0.16,
      0.17
    ]
  },
  {
    "name": "Коэфф. промокодов",
    "type": "dropdown",
    "items": [
      0.15,
      0.16,
      0.17
    ]
  },
  {
    "name": "Коэфф. оборачиваемости",
    "type": "dropdown",
    "items": [
      0.15,
      0.16,
      0.17
    ]
  }
]
 */
