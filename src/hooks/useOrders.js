import { useState } from "react";
import { instance } from "../api/api";

const useOrders = () => {
  const [orders, setOrders] = useState([]);

  const listOrders = async () => {
    const response = await instance.get("/orders");
    setOrders(response.data);
  };

  return [orders, listOrders];
};

export default useOrders;
