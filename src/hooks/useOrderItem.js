import { useState } from "react";
import { instance } from "../api/api";

const useOrderItem = () => {
  const [orderItem, setOrderItem] = useState("");

  const listOrderItem = async (id) => {
    const response = await instance.get(`/orders/${id}`);
    setOrderItem(response.data);
  };

  return [orderItem, listOrderItem];
};

export default useOrderItem;
