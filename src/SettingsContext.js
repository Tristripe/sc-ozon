import React from "react";

export const initialContextValue = {
  promo: 0,
  special: 0,
  code: 0,
  turn: 0,
  autoComplete: false,
  theshold: 0,
};

const SettingsContext = React.createContext(initialContextValue);

export default SettingsContext;
