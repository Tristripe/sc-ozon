import {store} from 'react-notifications-component';

const addNotification = (type, title, message) => {
	return store.addNotification({
		title,
		type,
		message,
		insert: "top",
		container: "top-right",
		animationIn: ["animate__animated", "animate__fadeIn"],
		animationOut: ["animate__animated", "animate__fadeOut"],
		dismiss: {
			duration: 3000,
			onScreen: true
		}
	});
}

export { addNotification }